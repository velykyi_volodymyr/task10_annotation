package com.velykyi.model;

import java.lang.reflect.Field;
import java.util.*;

public interface Model {
    List<Field> searchFields();
    void invokeMethods();
    void setValue(Object o);
    void invokeMyMethods();
}
