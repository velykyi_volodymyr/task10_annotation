package com.velykyi.model;

import com.velykyi.annotation.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Sequence {
    @MyAnnotation
    String lastName = "Velykyi";
    String firstName = "Volodymyr";
    @MyAnnotation(name = "Integer field")
    int age;
    private static Logger logger1 = LogManager.getLogger(Sequence.class);

    public int max(int el1, int el2, int el3){
        int max = Integer.max(Integer.max(el1,el2), el3);
        logger1.info("Method Sequence.max(el1, el2, el3) was worked.");
        return max;
    }

    private void printString(String str) {
        System.out.println(str);
        logger1.info("Method Sequence.printString(str) was worked.");
    }

    public String concatParams(){
        logger1.info("Method Sequence.concatParams() was worked.");
        return firstName + " " + lastName;
    }

    public void MyMethod(String str, String... strArray) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(str).append("\n");
        for (String string : strArray) {
            stringBuilder.append(string);
        }
        logger1.info("MyMethod(String str, String... array) was worked.");
        System.out.println(stringBuilder.toString());
    }

    public void MyMethod(String... strArray) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String string : strArray) {
            stringBuilder.append(string);
        }
        logger1.info("MyMethod(String... array) was worked.");
        System.out.println(stringBuilder.toString());
    }

}
