package com.velykyi.model;

import com.velykyi.annotation.MyAnnotation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class Task implements Model {
    Class clazz;
    private static Logger logger1 = LogManager.getLogger(Task.class);

    public Task(Class clazz) {
        this.clazz = clazz;

    }

    @Override
    public List<Field> searchFields() {
        Field[] fields = clazz.getDeclaredFields();
        List<Field> annotatedFields = new ArrayList<>();
        for (Field field : fields) {
            if (field.isAnnotationPresent(MyAnnotation.class)) {
                annotatedFields.add(field);
            }
        }
        return annotatedFields;
    }

    @Override
    public void invokeMethods() {
        Sequence sequence = new Sequence();
        Class clazz = sequence.getClass();
        try {
            Method method1 = clazz.getDeclaredMethod("max", int.class, int.class, int.class);
            Method method2 = clazz.getDeclaredMethod("printString", String.class);
            Method method3 = clazz.getDeclaredMethod("concatParams");
            int max = (int) method1.invoke(sequence, 1, 2, 3);
            method2.setAccessible(true);
            method2.invoke(sequence, "Hello, World");
            String concatParams = (String) method2.invoke(sequence);
        } catch (NoSuchMethodException e) {
            logger1.error("NoSuchMethodException");
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            logger1.error("IllegalAccessException");
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            logger1.error("InvocationTargetException");
            e.printStackTrace();
        }
    }

    @Override
    public void setValue(Object o) {
        Class clazzO = o.getClass();
        Field[] fields = clazzO.getDeclaredFields();
        fields[0].setAccessible(true);
        try {
            if (fields[0].getType() == int.class) {
                logger1.info("New int value was set.");
                fields[0].set(o, 47);
            } else if (fields[0].getType() == String.class) {
                logger1.info("New string value was set.");
                fields[0].set(o, "New value.");
            } else if (fields[0].getType() == double.class) {
                logger1.info("New double value was set.");
                fields[0].set(o, 0.47);
            } else if (fields[0].getType() == boolean.class) {
                logger1.info("New boolean value was set.");
                fields[0].set(o, false);
            }
        } catch (IllegalAccessException e) {
            logger1.error("IllegalAccessException");
            e.printStackTrace();
        }
    }

    @Override
    public void invokeMyMethods() {
        Sequence sequence = new Sequence();
        Class clazz = sequence.getClass();
        try {
            String[] str = {"my", "dear", "friend", "."};
            Method method1 = clazz.getDeclaredMethod("MyMethod", String.class, String[].class);
            Method method2 = clazz.getDeclaredMethod("MyMethod", String[].class);
            method1.invoke(sequence, "Hello,", str);
            method2.invoke(sequence, (Object) str);
        } catch (NoSuchMethodException e) {
            logger1.error("NoSuchMethodException");
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            logger1.error("IllegalAccessException");
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            logger1.error("InvocationTargetException");
            e.printStackTrace();
        }
    }
}
