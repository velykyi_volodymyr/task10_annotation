package com.velykyi.model;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;

public class UnknownClass {

    public void printInfo(Object o){
        Class clazz = o.getClass();
        System.out.println("Class name: " + clazz.getName());
        System.out.println("Class simple name: " + clazz.getSimpleName());
        Field[] fields = clazz.getDeclaredFields();
        System.out.println("Declared fields: ");
        for (Field field : fields){
            System.out.println(field.getName());
        }
        Method[] methods = clazz.getDeclaredMethods();
        System.out.println("Declared methods: ");
        for (Method method : methods){
            System.out.println(method.getName());
        }
        Annotation[] annotations = clazz.getAnnotations();
        System.out.println("Annotations: ");
        for (Annotation annotation : annotations){
            System.out.println(annotation.getClass().getName());
        }
    }
}
