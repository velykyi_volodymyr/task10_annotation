package com.velykyi;

import com.velykyi.view.View;

public class App {
    public static void main(String[] args) {
        View view = new View();
        view.showMenu();
    }
}
