package com.velykyi.view;

import com.velykyi.annotation.MyAnnotation;
import com.velykyi.model.Model;
import com.velykyi.model.Sequence;
import com.velykyi.model.Task;
import com.velykyi.model.UnknownClass;

import java.lang.reflect.Field;
import java.util.*;

public class View {
    private Map<String, String> menu;
    private Map<String, Printable> methodMenu;
    private Model model = new Task(Sequence.class);
    private static Scanner scann = new Scanner(System.in);
    private Locale locale;
    private ResourceBundle bundle;

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("7", bundle.getString("7"));
        menu.put("8", bundle.getString("8"));
        menu.put("9", bundle.getString("9"));
        menu.put("10", bundle.getString("10"));
        menu.put("q", bundle.getString("q"));
    }

    public View() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("menu", locale);
        setMenu();
        methodMenu = new LinkedHashMap<>();
        methodMenu.put("1", this::getAnnotatedFields);
        methodMenu.put("2", this::getAnnotationValue);
        methodMenu.put("3", this::invokeMethods);
        methodMenu.put("4", this::setFieldValue);
        methodMenu.put("5", this::invokeMyMethods);
        methodMenu.put("6", this::getClassInfo);
        methodMenu.put("7", this::setEnglish);
        methodMenu.put("8", this::setUkrainian);
        methodMenu.put("9", this::setGerman);
        methodMenu.put("10", this::setGreek);
    }

    private void getClassInfo() {
        UnknownClass unknownClass = new UnknownClass();
        unknownClass.printInfo(new Sequence());
    }

    private void invokeMyMethods() {
        model.invokeMyMethods();
    }

    private void setFieldValue() {
        model.setValue(new Sequence());
    }

    private void invokeMethods() {
        model.invokeMethods();
    }

    private void getAnnotationValue() {
        List<Field> fields = model.searchFields();
        for (Field field : fields) {
            MyAnnotation annotation = field.getAnnotation(MyAnnotation.class);
            System.out.println("Field: " + field.getName() + " name in @MyAnnotation: " + annotation.name());
        }
    }

    private void getAnnotatedFields() {
        List<Field> fields = model.searchFields();
        for (Field field : fields) {
            System.out.println("Field: " + field.getName());
        }
    }

    private void setEnglish() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("menu", locale);
        setMenu();
    }

    private void setUkrainian() {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("menu", locale);
        setMenu();
    }

    private void setGerman() {
        locale = new Locale("de");
        bundle = ResourceBundle.getBundle("menu", locale);
        setMenu();
    }

    private void setGreek() {
        locale = new Locale("el");
        bundle = ResourceBundle.getBundle("menu", locale);
        setMenu();
    }

    public void showMenu() {
        String keyMenu;
        do {
            System.out.println("\nMENU:");
            for (String str : menu.values()) {
                System.out.println(str);
            }
            System.out.println("Please, select menu point.");
            keyMenu = scann.nextLine().toUpperCase();
            try {
                methodMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.toUpperCase().equals("Q"));
    }

}
